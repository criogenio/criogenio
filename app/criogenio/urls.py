#encode: utf-8

from django.conf.urls import url, include
from django.contrib import admin

from tastypie.api import Api

from presentations.api import UserResource, PresentationResource

presentation_resource = PresentationResource()

v1_api = Api(api_name='v1')

v1_api.register(UserResource())
v1_api.register(PresentationResource())


urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^api/', include(v1_api.urls)),
]
