#encode:utf-8

from django.contrib.auth.models import User
from django.db import IntegrityError
from tastypie import fields
from tastypie.resources import ModelResource, ALL, ALL_WITH_RELATIONS
from tastypie.authorization import Authorization
from tastypie.exceptions import BadRequest

from .models import Presentation, UserProfile


class ProfileResource(ModelResource):
    class Meta:
        queryset = UserProfile.objects.all()
        resource_name = 'userprofile'
        authorization=Authorization()
        include_resource_uri = True


class UserResource(ModelResource):
    profile = fields.ToOneField(ProfileResource, attribute='profile', related_name='user', full=True, null=True)
    class Meta:
        queryset = User.objects.all()
        resource_name = 'user'
        authorization=Authorization()
        include_resource_uri = True
        fields = ['id', 'username', 'first_name', 'last_name', 'profile']
        filtering = {
            'username': ALL,
            'profile': ALL_WITH_RELATIONS,
            'id': ALL
        }
    def obj_create(self, bundle, request=None, **kwargs):
        try:
            bundle = super(UserResource, self).obj_create(bundle)
            bundle.obj.set_password(bundle.data.get('password'))
            bundle.obj.save()
            profile, created = UserProfile.objects.get_or_create(userprofile=bundle.obj)
            profile.prezi_profile_url = bundle.data.get("userprofile").get("prezi_profile_url")
            profile.save()
        except IntegrityError:
            raise BadRequest('Username already exists')
        return bundle


class PresentationResource(ModelResource):
    created_by = fields.ForeignKey(UserResource, 'created_by', full=True)
    class Meta:
        queryset = Presentation.objects.all()
        resource_name = 'presentation'
        authorization=Authorization()
        include_resource_uri = True
        filtering = {
            'created_at': ['exact', 'lt', 'lte', 'gte', 'gt'],
            'title': ALL,
        }
        ordering = ['created_at']
