#encode:utf-8

from datetime import datetime, timedelta
from django.contrib.auth.models import User
from django.utils import timezone

import factory


class UserFactory(factory.Factory):
    class Meta:
        model = User

    username = factory.Sequence(lambda x: 'user-%d' % x)
    first_name = "Marcius"
    last_name = "Oliveira"

    @factory.post_generation
    def create_presentations(self, create, extracted, **kwargs):
        if not create:
            return
        self.save()
        for i in range(10):
            presentations = PresentationFactory.create(created_by=self)


class UserProfileFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = 'presentations.UserProfile'

    user = factory.SubFactory(UserFactory)
    prezi_profile_url = factory.Sequence(lambda x: 'www.site.com/%d' % x)


class PresentationFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = 'presentations.Presentation'

    title = factory.Sequence(lambda x: 'Presentation - %d' % x)
    created_at = factory.Sequence(lambda n:(timezone.now() + timedelta(days=n))\
                                .date(), int)
    created_by = factory.SubFactory(UserFactory)
