#encode: utf-8
import os
import json
from dateutil import parser as dateparser

from optparse import make_option
from django.contrib.auth.models import User
from django.core.management.base import BaseCommand, CommandError
from django.utils.text import slugify
from presentations.models import Presentation

class Command(BaseCommand):
    help = u'Import JSON file to DB'

    def add_arguments(self, parser):
        parser.add_argument('file_path', nargs='+', type=str)

    def handle(self, *args, **options):
        file_path = options.get('file_path')
        if not file_path:
            print(u"You must specify a JSON file.")
            return
        for path in file_path:
            fn = os.path.join(os.path.dirname(__file__), path)
            try:
                with open(fn) as data_file:
                    presentation_set = json.load(data_file)
                    self.create_presentations(presentation_set)
            except IOError as e:
                print("The file specified does not found.")

    def create_profile(self, creator_data):
        username = slugify(creator_data.get('name'))
        all_names = creator_data.get('name').split(" ")
        first_name = all_names[0]
        last_name = all_names[1]
        user, created = User.objects.get_or_create(username=username,
                first_name=first_name,
                last_name=last_name, defaults={
                'email': "%s@prezi.com" % username,
                'first_name': first_name,
                'last_name': last_name})
        user.profile.prezi_profile_url = creator_data.get('profileUrl')
        user.profile.save()
        if created:
            user.set_password("%s-123change" % username)
            user.save()
        return user

    def create_presentations(self, presentation_set):
        try:
            for presentation_data in presentation_set:
                user = self.create_profile(presentation_data.get('creator'))
                presentation, created = Presentation.objects.get_or_create(
                                            prezi_id=presentation_data.get('id'),
                                            created_by=user)
                presentation.title = presentation_data.get('title')
                presentation.thumbnail = presentation_data.get('thumbnail')
                presentation.prezi_id = presentation_data.get('id')
                presentation.created_at = dateparser.parse(
                                                presentation_data.get('createdAt'))
                presentation.save()
        except Exception as e:
            print e
