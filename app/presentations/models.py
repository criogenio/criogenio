#encode: utf-8
from __future__ import unicode_literals

from datetime import datetime
from django.utils import timezone
from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.core.files.storage import FileSystemStorage


class UserProfile(models.Model):
    userprofile = models.OneToOneField(User, related_name="profile")
    prezi_profile_url = models.CharField(max_length=255, null=True)


class Presentation(models.Model):
    title = models.CharField(max_length=255)
    created_at = models.DateTimeField(default=timezone.now)
    created_by = models.ForeignKey(User, on_delete=models.CASCADE)
    thumbnail = models.CharField(max_length=255, default="https://placeimg.com/400/400/any")
    prezi_id = models.CharField(max_length=255, unique=True, null=True)

def create_user_profile(sender, instance, created, **kwargs):
    if created:
        profile, created = UserProfile.objects.get_or_create(userprofile=instance)

post_save.connect(create_user_profile, sender=User)
