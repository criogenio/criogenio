#encode: utf-8

import json
from django.test import TestCase
from django.contrib.auth.models import User
from tastypie.test import TestApiClient
from tastypie.test import ResourceTestCaseMixin
from tastypie.serializers import Serializer
from presentations.models import Presentation


class UserProfileHandlerTestCase(ResourceTestCaseMixin, TestCase):

    def post_new_to_test(self):
        user_data = {
            'username': u'foobaa',
            'first_name': u'Marcius',
            'last_name': u'Oliveira',
            'email': u'marciusoliveiraa@gmail.com',
            'password': u'password',
            'userprofile': {
                'prezi_profile_url': u"fooblah.com"
            }
        }
        resp = self.api_client.post('/api/v1/user', data=user_data,
                                headers={"content-type": "application/json"})

    def test_post_new_user(self):
        user_data = {
            'username': u'foobaa',
            'first_name': u'Marcius',
            'last_name': u'Oliveira',
            'email': u'marciusoliveiraa@gmail.com',
            'password': u'password',
            'userprofile': {
                'prezi_profile_url': u"fooblah.com"
            }
        }

        resp = self.api_client.post('/api/v1/user', data=user_data,
                                headers={"content-type": "application/json"})
        self.assertEquals(resp.status_code, 201)

    def test_post_update_user(self):
        self.post_new_to_test()
        user_data = {
            'username': u'foobaa2',
            'first_name': u'Marcius',
            'last_name': u'Oliveira',
            'email': u'marciusoliveiraad@gmail.com',
            'password': u'password',
            'userprofile': {
                'prezi_profile_url': u'http://prezi.com/id/12355'
            }
        }

        resp = self.api_client.post('/api/v1/user', data=user_data,
            headers={"content-type": "application/json"})
        self.assertEquals(resp.status_code, 201)
        user_data['username'] = u'fooofooo'
        resp = self.api_client.put('/api/v1/user/1', data=user_data,
            headers={"content-type": "application/json"})
        self.assertEquals(resp.status_code, 201)

    def test_get_list_json(self):
        self.post_new_to_test()
        resp = self.api_client.get('/api/v1/user')
        self.assertValidJSONResponse(resp)
        self.assertEqual(len(self.deserialize(resp)['objects']), 1)
        self.assertEqual(self.deserialize(resp)['objects'][0]['username'],
                        'foobaa')


class PresentationHandlerTestCase(ResourceTestCaseMixin, TestCase):
    def create_user(self):
        user_data = {
            'username': u'foobaa2',
            'first_name': u'Marcius',
            'last_name': u'Oliveira',
            'email': u'marciusoliveiraad@gmail.com',
            'password': u'password',
            'userprofile': {
                'prezi_profile_url': u'http://prezi.com/id/12355'
            }
        }

        resp = self.api_client.post('/api/v1/user', data=user_data,
            headers={"content-type": "application/json"})

    def test_post_new_presentation(self):
        self.create_user()
        presentation_data = {
            'title': u'foobaa',
            'created_at': '2016-5-21 10:00',
            'created_by': "/api/v1/user/1",
            'prezi_id': None,
        }

        resp = self.api_client.post('/api/v1/presentation', data=presentation_data,
                                headers={"content-type": "application/json"})
        print resp.content
        self.assertEquals(resp.status_code, 201)
