#!/bin/bash
python manage.py migrate                  # Apply database migrations
#python manage.py collectstatic --noinput  # Collect static files
python manage.py json_import_db /tmp/prezis.json #Import json
