define([
  'backbone',
  'routers/presentationRoutes'
], function(Backbone, PresentationRoutes){
  var initialize = function(){
    var routes = new PresentationRoutes();
  }
  return {
    initialize: initialize
  };
});
