define([
  'backbone',
  'models/presentation'
], function(Backbone, Presentation){
  var PresentationCollections = Backbone.Collection.extend({
    model: Presentation,
    url: function(){
      return this.baseUrl + "/api/v1/presentation";
    },
    initialize: function(){
      this.baseUrl =  "http://criogenio.com:8000";
      this.last_filter = {};
      this.next = "";
    },
    parse: function(response){
        this.recent_meta = response.meta || {};
        return response.objects || response;
    },
    doFilter: function(params, callback){
      var self = this;
      if (params.order_by)
        this.last_filter.order_by = params.order_by;
      else
        this.last_filter = params;

      this.fetch({
        dataType: 'jsonp',
        data: this.last_filter,
        processData: true,
        reset : true
      });
    },

    nextPage: function(callback){
      if(!this.recent_meta.next)
        return callback.error(null);
      var self = this;
      var url = this.baseUrl + this.recent_meta.next
      this.fetch({
        url: url,
        dataType: 'jsonp',
        success(collection, response){
          self.parse(response);
          return callback.success(collection);
        },
        error(error, response){
          return callback.error(error);
        }
      });
    }
  });
  return PresentationCollections;
});
