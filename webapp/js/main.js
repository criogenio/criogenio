require.config({
  paths: {
    text: 'lib/text',
    jquery: 'lib/jquery-min',
    underscore: 'lib/underscore-min',
    backbone: 'lib/backbone-min',
    moment: 'lib/moment.min',
    bootstrap: 'lib/bootstrap-min'
  }
});

define(['app'], function(App){
  App.initialize();
});
