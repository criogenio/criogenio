define([
  'underscore',
  'backbone'
], function(_, Backbone){
  var Presentation = Backbone.Model.extend({
    url:'http://criogenio.com/api/v1/presentation'
  });
  return Presentation;
});
