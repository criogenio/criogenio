define([
  'backbone',
  'views/presentationView',
  'views/searchView',
  'views/dateOrderView',
  'collections/presentationCollections'
], function(Backbone, PresentationView, SearchView, DateOrderView, PresentationCollections){

  var PresentationRoutes = Backbone.Router.extend({
    initialize: function(){
      Backbone.history.start({ pushState: true });
    },
    routes: {
      "": "rootRouter",
    },
    rootRouter: function(){
      var collection = new PresentationCollections();
      var view = new PresentationView({ collection: collection });
      var dateOrderView = new DateOrderView({ collection: collection });
      var searchView = new SearchView({ collection: collection });
    }
  });
  return PresentationRoutes;
});
