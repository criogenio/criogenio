
<div class="col-sm-6 col-md-4">
   <div class="card">
       <div class="card-image">
           <img class="img-responsive" alt="<%= title %>" src="<%= thumbnail %>">
           <span class="card-title"><%= title %></span>
       </div>

       <div class="card-content">
          <a href="<%= created_by.profile.prezi_profile_url %>" target="_blank" class="glyphicon glyphicon-user"> <%= created_by.first_name %> <%= created_by.last_name %></a>
          <p><span class="glyphicon glyphicon-time"></span> <%= created_at.format("MMM D, YYYY") %></p>
       </div>

       <div class="card-action">
           <a href="http://prezi.com/<%= prezi_id %>/?utm_campaign=share&utm_medium=copy&rc=ex0share" target="_blank">GO TO PREZI</a>
       </div>
   </div>
 </div>
