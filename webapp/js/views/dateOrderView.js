define([
  'underscore',
  'jquery',
  'backbone',
  'text!templates/dateOrderTemplate.tpl'], function(_, $, Backbone, Template){
    var DatePickerView = Backbone.View.extend({
      el: '.order',
      events: {
        'click button#orderdate': 'orderCreated'
      },
      initialize: function(){
        this.template = _.template(Template);
        this.desc = true
        this.render();
      },
      orderCreated: function(e){
        e.preventDefault();
        this.desc = !this.desc;
        var order_by = this.desc ? "-created_at" : "created_at";
        e.target.className = this.desc ? "glyphicon glyphicon-chevron-up" : "glyphicon glyphicon-chevron-down";
        this.collection.doFilter({order_by:order_by}, {
          success: function(collection){

          }
      });
      },
      render: function(){
        this.$el.append(this.template());
        return this;
      }
    });
    return DatePickerView;
});
