define([
  'jquery',
  'underscore',
  'backbone',
  'moment',
  'text!templates/presentationItemTemplate.tpl'
], function($, _, Backbone, Moment, Template){

  var PresentationView = Backbone.View.extend({
      el: ".row",
      initialize: function(options){
        this.filterData = { order_by: "-created_at" };
        this.template = _.template(Template);
        //bind to window
       _.bindAll(this, 'detectScroll');
       $(window).scroll(this.detectScroll);
        //listenTo reset collection
        this.listenTo(this.collection, 'reset', this.addAll, this);
        this.render();
      },
      addOne: function(model) {
        var modelJSON = model.toJSON();
        modelJSON.created_at = Moment(modelJSON.created_at);
    		var view = this.template(modelJSON)
    		this.$el.append(view);
    	},
    	addAll: function(){
    		var self = this;
        this.$el.html("");
        this.collection.each(function(model){
          self.addOne(model);
        });
    	},
      detectScroll: function(){
        var self = this;
        if($(window).scrollTop() + $(window).height() == $(document).height()){
            self.collection.nextPage({
             success: function(collection){
               collection.each(function(model){
                 self.addOne(model);
               });
           },
           error: function(message){
             console.log(message);
           }
         });
        }
      },
    	render: function() {
        var self = this;
        this.collection.doFilter(self.filterData, {
          success: function(collection, response) {
            self.addAll();
        }});
        return this;
    	}
    });
    return PresentationView;
});
