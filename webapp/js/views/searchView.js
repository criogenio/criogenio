define([
  'underscore',
  'jquery',
  'backbone',
  'text!templates/searchTemplate.tpl'], function(_, $, Backbone, Template){
    var SearchView = Backbone.View.extend({
      el: '.search',
      events: {
        'submit #search-form': 'applySearch'
      },
      initialize: function(){
        this.template = _.template(Template);
        this.render();
      },

      applySearch: function(e){
        e.preventDefault();
        var self = this;
        var content = $.trim(this.$('#q').val()) || null;
        var data = { "title__icontains": content };
        if(content === null){
            data = { };
        }
        this.collection.doFilter(data, {
            success: function(response, collection){
              self.collection.reset(self.collection.parse(collection));
            }
          });
        },

      render: function(){
        this.$el.append(this.template());
        return this;
      }
    });
    return SearchView;
});
